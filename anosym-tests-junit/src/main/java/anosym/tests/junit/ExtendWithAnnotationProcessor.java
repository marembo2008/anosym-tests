package anosym.tests.junit;

import org.atteo.classindex.processor.ClassIndexProcessor;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 4, 2018, 9:08:00 PM
 */
public class ExtendWithAnnotationProcessor extends ClassIndexProcessor {

  public ExtendWithAnnotationProcessor() {
    indexAnnotations(ExtendWith.class);
  }

}
