package anosym.tests.wiremock;

import java.util.Random;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 24, 2020, 12:00:22 AM
 */
@Slf4j
public class WiremockExtension extends WireMockServer
		implements BeforeAllCallback, AfterAllCallback, BeforeEachCallback, AfterEachCallback {

	public static final String SERVER_PORT_PROPERTY = "wiremock.api.port";

	private static final int MIN_PORT = 10000;

	public WiremockExtension() {
		super(randomPort());
	}

	@Override
	public void beforeAll(ExtensionContext ec) throws Exception {
		log.info("Starting server on port: {}", ((WireMockConfiguration) getOptions()).portNumber());
		this.start();
		WireMock.configureFor(this.client);
	}

	@Override
	public void afterAll(ExtensionContext ec) throws Exception {
		log.info("Stoping server on port: {}", ((WireMockConfiguration) getOptions()).portNumber());
		this.stop();
		this.resetAll();
	}

	@Override
	public void beforeEach(ExtensionContext ec) throws Exception {
	}

	@Override
	public void afterEach(ExtensionContext ec) throws Exception {
		this.resetAll();
	}

	private static int randomPort() {
		final int port = (new Random()).nextInt(50000) + MIN_PORT;
		System.setProperty(SERVER_PORT_PROPERTY, port + "");
		
		return port;
	}

}
