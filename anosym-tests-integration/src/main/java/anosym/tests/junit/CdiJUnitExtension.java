package anosym.tests.junit;

import java.util.Arrays;

import jakarta.enterprise.inject.spi.CDI;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestInstanceFactory;
import org.junit.jupiter.api.extension.TestInstanceFactoryContext;
import org.junit.jupiter.api.extension.TestInstantiationException;

import anosym.tests.cdi.ActiveProfile;
import anosym.tests.cdi.container.CdiContainerInstance;

import static java.util.stream.Collectors.joining;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 4, 2018, 6:09:04 AM
 */
@Slf4j
public class CdiJUnitExtension implements TestInstanceFactory, BeforeAllCallback, AfterAllCallback {

  private static final String ACTIVE_PROFILES_KEY = "active.profiles";

  @Override
  public Object createTestInstance(final TestInstanceFactoryContext tifc, final ExtensionContext ec) throws TestInstantiationException {
    log.info("Initializing cdi junit bean for class: {}", tifc.getTestClass());
    return CDI.current().select(tifc.getTestClass()).get();
  }

  @Override
  public void beforeAll(final ExtensionContext ec) throws Exception {
    final ExtensionContext.Namespace methodNamespace = ExtensionContext.Namespace.create(ec.getTestInstance());
    final Class<?> testClass = ec.getTestClass().get();
    final String activeProfiles = Arrays.stream(testClass.getAnnotationsByType(ActiveProfile.class))
            .map(ActiveProfile::value)
            .flatMap(Arrays::stream)
            .collect(joining(","));
    System.setProperty(ACTIVE_PROFILES_KEY, activeProfiles);

    log.info("Active profiles: {}", activeProfiles);

    final CdiContainerInstance instance = new CdiContainerInstance();
    instance.initialize();
    ec.getStore(methodNamespace).put("cdi-container", instance);
  }

  @Override
  public void afterAll(final ExtensionContext ec) throws Exception {
    final ExtensionContext.Namespace methodNamespace = ExtensionContext.Namespace.create(ec.getTestInstance());
    final CdiContainerInstance instance = ec.getStore(methodNamespace).get("cdi-container", CdiContainerInstance.class);
    if (instance != null) {
      instance.close();
    }

    System.clearProperty(ACTIVE_PROFILES_KEY);
  }

}
