package anosym.tests.junit;

import java.lang.reflect.Method;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import anosym.tests.cassandra.CassandraServer;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 4, 2018, 6:09:04 AM
 */
@Slf4j
public class CassandraJUnitExtension implements BeforeAllCallback, AfterAllCallback, BeforeTestExecutionCallback, AfterTestExecutionCallback {

  @Override
  public void beforeAll(final ExtensionContext ec) throws Exception {
    final ExtensionContext.Namespace serverKey = ExtensionContext.Namespace.create(ec.getTestInstance());
    final CassandraServer server = new CassandraServer();
    ec.getStore(serverKey).put("cassandra-server", server);
  }

  @Override
  public void afterAll(final ExtensionContext ec) throws Exception {
    final ExtensionContext.Namespace serverKey = ExtensionContext.Namespace.create(ec.getTestInstance());
    final CassandraServer server = ec.getStore(serverKey).get("cassandra-server", CassandraServer.class);
    if (server != null) {
      server.close();
    }
  }

  @Override
  public void beforeTestExecution(final ExtensionContext ec) throws Exception {
    final ExtensionContext.Namespace serverKey = ExtensionContext.Namespace.create(ec.getTestInstance());
    final CassandraServer server = ec.getStore(serverKey).get("cassandra-server", CassandraServer.class);
    if (server != null) {
      final Method method = ec.getRequiredTestMethod();
      server.beforeMethodExecution(method);
    }
  }

  @Override
  public void afterTestExecution(final ExtensionContext ec) throws Exception {
    final ExtensionContext.Namespace serverKey = ExtensionContext.Namespace.create(ec.getTestInstance());
    final CassandraServer server = ec.getStore(serverKey).get("cassandra-server", CassandraServer.class);
    if (server != null) {
      final Method method = ec.getRequiredTestMethod();
      server.afterMethodExecution(method);
    }
  }

}
