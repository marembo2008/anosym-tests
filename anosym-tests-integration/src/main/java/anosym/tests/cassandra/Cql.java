package anosym.tests.cassandra;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import anosym.tests.cassandra.Cql.Cqls;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 3, 2018, 9:26:43 PM
 */
@Repeatable(Cqls.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Cql {

    String resourcePath();

    ExecutionPhase phase();

    enum ExecutionPhase {

        BEFORE_TEST_METHOD,
        AFTER_TEST_METHOD;

    }

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface Cqls {

        Cql[] value();

    }

}
