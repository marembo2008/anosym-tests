package anosym.tests.cassandra;

import static anosym.tests.cassandra.Cql.ExecutionPhase.AFTER_TEST_METHOD;
import static anosym.tests.cassandra.Cql.ExecutionPhase.BEFORE_TEST_METHOD;
import static anosym.tests.cassandra.EmbeddedConfigurationLoader.EmbeddedConfiguration.INSTANCE;

import java.io.File;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Nullable;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.metadata.EndPoint;

import anosym.tests.cassandra.Cql.Cqls;
import anosym.tests.cassandra.Cql.ExecutionPhase;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.cassandra.service.CassandraDaemon;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 3, 2018, 6:12:59 AM
 */
@Slf4j
public class CassandraServer implements AutoCloseable {

  static {
    System.setProperty("cassandra.config.loader", EmbeddedConfigurationLoader.class.getCanonicalName());
  }

  private final CassandraInstance instance;

  private CqlSession session;

  public CassandraServer() throws Exception {
    instance = new CassandraInstance();
    try {
      instance.init();
    } catch (final Exception ex) {
      log.error("Error starting cassandra server", ex);
      instance.close();
      throw ex;
    }
  }

  @Override
  public void close() {
    try {
      instance.close();
    } catch (final Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  public void beforeMethodExecution(@NonNull final Method method) throws Exception {
    initCqls(method);
  }

  public void afterMethodExecution(@NonNull final Method frameworkMethod) throws Exception {
    cleanCql(frameworkMethod);
  }

  @Nullable
  private Cql getCql(@NonNull final Method method, @NonNull final ExecutionPhase phase) {
    final Cqls cqls = method.isAnnotationPresent(Cqls.class)
            ? method.getAnnotation(Cqls.class) : method.getDeclaringClass().getAnnotation(Cqls.class);
    if (cqls == null) {
      return null;
    }

    return Arrays
            .stream(cqls.value())
            .filter((cql) -> cql.phase() == phase)
            .findFirst()
            .orElse(null);
  }

  private void initCqls(@NonNull final Method method) throws Exception {
    final Cql beforeCql = getCql(method, BEFORE_TEST_METHOD);
    if (beforeCql == null) {
      return;
    }

    final EndPoint endPoint = new EndPoint() {

      @Override
      public String asMetricPrefix() {
        return "localhost-integration-test";
      }

      @Override
      public SocketAddress resolve() {
        return new InetSocketAddress("127.0.0.1", 58000);
      }

    };

    if (session == null) {
      session = CqlSession.builder()
              .addContactEndPoints(List.of(endPoint))
              .build();
    }

    final Path cqlPath = Paths.get(getClass().getResource(beforeCql.resourcePath()).toURI());
    Files.lines(cqlPath)
            .map(String::trim)
            .filter((script) -> !script.isEmpty())
            .map(this::sanitizeScript)
            .peek((script) -> log.info("Executing script <{}>", script))
            .forEach(session::execute);
  }

  private void cleanCql(@NonNull final Method method) throws Exception {
    if (session == null) {
      return;
    }

    final Cql afterCql = getCql(method, AFTER_TEST_METHOD);
    if (afterCql == null) {
      return;
    }

    final Path cqlPath = Paths.get(getClass().getResource(afterCql.resourcePath()).toURI());
    Files.lines(cqlPath)
            .map(String::trim)
            .filter((script) -> !script.isEmpty())
            .map(this::sanitizeScript)
            .peek((script) -> log.info("Executing script <{}>", script))
            .forEach(session::execute);
  }

  @NonNull
  private String sanitizeScript(@NonNull final String script) {
    final int ix = script.lastIndexOf(";");
    if (ix == script.length() - 1) {
      return script.substring(0, script.length() - 1);
    }

    return script;
  }

  private static class CassandraInstance implements AutoCloseable {

    private final CassandraDaemon cassandraDaemon;

    private CassandraInstance() {
      cassandraDaemon = new CassandraDaemon(true);
    }

    public void init() {
      cassandraDaemon.activate();
    }

    @Override
    public void close() throws Exception {
      cleanUpFiles();
      cassandraDaemon.deactivate();
      cleanUpFiles();
    }

    private void cleanUpFiles() throws Exception {
      final Path rootPath = Paths.get(INSTANCE.getConfig().cdc_raw_directory);
      Files.walk(rootPath, FileVisitOption.FOLLOW_LINKS)
              .sorted(Comparator.reverseOrder())
              .map(Path::toFile)
              .filter(File::exists)
              .peek((path) -> log.debug("Deleting cassandra temp dir: {}", path))
              .forEach(File::delete);
    }

  }

}
