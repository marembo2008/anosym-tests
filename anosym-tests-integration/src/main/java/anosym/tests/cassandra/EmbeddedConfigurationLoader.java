package anosym.tests.cassandra;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.Getter;
import lombok.NonNull;
import org.apache.cassandra.config.Config;
import org.apache.cassandra.config.ConfigurationLoader;
import org.apache.cassandra.config.ParameterizedClass;
import org.apache.cassandra.exceptions.ConfigurationException;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 3, 2018, 6:20:25 AM
 */
public class EmbeddedConfigurationLoader implements ConfigurationLoader {

    private static final ObjectMapper YAML_MAPPER = new ObjectMapper(new YAMLFactory())
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Override
    public Config loadConfig() throws ConfigurationException {
        return EmbeddedConfiguration.INSTANCE.config;
    }

    static class EmbeddedConfig extends Config {

        public EmbeddedParameterizedClass embedded_seed_provider;

    }

    static class EmbeddedParameterizedClass extends ParameterizedClass {

        @JsonCreator
        public EmbeddedParameterizedClass(@JsonProperty("class_name") final String className,
                                          @JsonProperty("parameters") final Map<String, String> parameters) {
            super(className, parameters);
        }

    }

    static enum EmbeddedConfiguration {

        INSTANCE;

        @Getter
        @NonNull
        private final EmbeddedConfig config;

        private EmbeddedConfiguration() {
            final InputStream configSource = getClass().getResourceAsStream("/cassandra.yml");
            try {
                this.config = YAML_MAPPER.readValue(configSource, EmbeddedConfig.class);

                //default seed_provider is not jackson friendly.
                this.config.seed_provider = this.config.embedded_seed_provider;

                //create all directories
                final Path cassandraDir = Paths.get(config.cdc_raw_directory);
                Files.createDirectories(cassandraDir);
                Files.createDirectories(Paths.get(config.commitlog_directory));
                Files.createDirectories(Paths.get(config.hints_directory));
                Files.createDirectories(Paths.get(config.saved_caches_directory));
            } catch (final IOException ex) {
                throw new RuntimeException(ex);
            }
        }

    }

}
