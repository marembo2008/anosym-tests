package anosym.tests.cdi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.spi.CreationalContext;
import jakarta.enterprise.inject.spi.AnnotatedType;
import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.enterprise.inject.spi.InjectionTarget;
import jakarta.enterprise.inject.spi.InjectionTargetFactory;
import jakarta.enterprise.inject.spi.configurator.AnnotatedFieldConfigurator;
import jakarta.inject.Inject;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import static java.util.stream.Collectors.toSet;

import static com.google.common.base.Preconditions.checkState;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 4, 2018, 6:47:24 AM
 */
@Slf4j
public class JUnitBean<T> implements Bean<T> {

  @Nonnull
  private final Class<?> junitClass;

  private final InjectionTargetFactory<T> injectionTargetFactory;

  private final Set<InjectionPoint> injectionPoints;

  private final AtomicReference<InjectionTarget<T>> injectionTarget = new AtomicReference<>();

  public JUnitBean(@NonNull final Class<T> junitClass, @NonNull final BeanManager beanManager) {
    this.junitClass = junitClass;

    final AnnotatedType<T> junitAt = beanManager.createAnnotatedType(junitClass);
    this.injectionTargetFactory = beanManager.getInjectionTargetFactory(junitAt);
    this.injectionPoints = injectionTargetFactory.configure()
            .fields()
            .stream()
            .map(AnnotatedFieldConfigurator::getAnnotated)
            .filter((field) -> field.isAnnotationPresent(Inject.class))
            .filter((field) -> !field.isStatic())
            .map(beanManager::createInjectionPoint)
            .collect(toSet());

    log.info("Resolved injection points <{}>: {}", injectionPoints.size(), injectionPoints);
  }

  @Override
  public Class<?> getBeanClass() {
    return junitClass;
  }

  @Override
  public Set<InjectionPoint> getInjectionPoints() {
    return injectionPoints;
  }

  @Override
  public boolean isNullable() {
    return false;
  }

  @Override
  public T create(final CreationalContext<T> creationalContext) {
    final InjectionTarget<T> junitIt = resolveInjectionTarget();
    final T instance = junitIt.produce(creationalContext);
    junitIt.inject(instance, creationalContext);
    junitIt.postConstruct(instance);

    return instance;
  }

  @Override
  public void destroy(final T instance, final CreationalContext<T> creationalContext) {
    final InjectionTarget<T> junitIt = resolveInjectionTarget();
    junitIt.preDestroy(instance);
    junitIt.dispose(instance);
    creationalContext.release();
  }

  @Override
  public Set<Type> getTypes() {
    return ImmutableSet.of(junitClass, Object.class);
  }

  @Override
  public Set<Annotation> getQualifiers() {
    return ImmutableSet.of(new DefaultAnnotation(), new AnyAnnotation());
  }

  @Override
  public Class<? extends Annotation> getScope() {
    return ApplicationScoped.class;
  }

  @Override
  public String getName() {
    return junitClass.getSimpleName();
  }

  @Override
  public Set<Class<? extends Annotation>> getStereotypes() {
    return ImmutableSet.of();
  }

  @Override
  public boolean isAlternative() {
    return false;
  }

  @Nonnull
  private InjectionTarget<T> resolveInjectionTarget() {
    if (injectionTarget.get() != null) {
      return injectionTarget.get();
    }

    final InjectionTarget<T> newInjectionTarget = injectionTargetFactory.createInjectionTarget(this);
    checkState(newInjectionTarget != null, "Could not resolve injection target for bean: <%s>", junitClass);

    injectionTarget.set(newInjectionTarget);

    return newInjectionTarget;
  }

}
