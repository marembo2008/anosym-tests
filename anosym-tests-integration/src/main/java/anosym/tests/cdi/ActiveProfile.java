package anosym.tests.cdi;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 24, 2020, 12:51:49 AM
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ActiveProfiles.class)
public @interface ActiveProfile {

  /**
   * profiles to activate
   */
  String[] value();

}
