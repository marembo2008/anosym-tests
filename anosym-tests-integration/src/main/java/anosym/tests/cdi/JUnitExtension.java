package anosym.tests.cdi;

import static java.util.stream.Collectors.toSet;

import java.util.Arrays;
import java.util.Set;

import org.atteo.classindex.ClassIndex;
import org.junit.jupiter.api.extension.ExtendWith;

import com.google.common.collect.ImmutableSet;

import anosym.tests.junit.CdiJUnitExtension;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.spi.AfterBeanDiscovery;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.enterprise.inject.spi.Extension;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 4, 2018, 6:48:18 AM
 */
@Slf4j
public class JUnitExtension implements Extension {

  private static final Set<Class<?>> JUNIT_CLASSES;

  static {
    JUNIT_CLASSES = ImmutableSet
            .copyOf(ClassIndex.getAnnotated(ExtendWith.class))
            .stream()
            .filter((cls) -> {
              final ExtendWith[] extendWiths = cls.getAnnotationsByType(ExtendWith.class);
              return Arrays.stream(extendWiths)
                      .map(ExtendWith::value)
                      .flatMap(Arrays::stream)
                      .anyMatch(CdiJUnitExtension.class::isAssignableFrom);
            })
            .peek((junitClass) -> log.info("--- Found <{}> ", junitClass.getCanonicalName()))
            .collect(toSet());
  }

  void afterBeanDiscovery(@Observes final AfterBeanDiscovery afterBeanDiscovery, final BeanManager beanManager) {
    log.info("Registering junit test beans..............: {}", JUNIT_CLASSES);

    JUNIT_CLASSES
            .stream()
            .map((junitClass) -> new JUnitBean(junitClass, beanManager))
            .forEach(afterBeanDiscovery::addBean);
  }

}
