package anosym.tests.cdi.container;

import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Apr 17, 2018, 10:51:20 PM
 */
@Slf4j
public class CdiContainerInstance implements AutoCloseable {

  private SeContainer seContainer;

  public void initialize() {
    final SeContainerInitializer initializer = SeContainerInitializer.newInstance();
    this.seContainer = initializer.initialize();
  }

  @Override
  public void close() {
    log.info("shutting cdi container...................");
    if (seContainer != null) {
      seContainer.close();
    }
  }

}
