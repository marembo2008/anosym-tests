package anosym.tests;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.apache.cassandra.concurrent.NamedThreadFactory;

import anosym.async.executor.ManagedExecutorContextProvider;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.Specializes;

/**
 *
 * @author marembo
 */
@ApplicationScoped
public class TestManagedExecutorContextProvider extends ManagedExecutorContextProvider {

  @Override
  @Default
  @Produces
  @Specializes
  @ApplicationScoped
  public ThreadFactory getManagedThreadFactory() {
    return new NamedThreadFactory("test-thread-factory");
  }

  @Override
  @Default
  @Produces
  @Specializes
  @ApplicationScoped
  public ExecutorService getManagedExecutorService() {
    return Executors.newSingleThreadExecutor();
  }

}
